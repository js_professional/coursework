import '../public/css/style.css';
import app from './commands';

document.addEventListener('DOMContentLoaded', () => {
    app();
});
