import {
    CommandEdit,
    TextCommand,
    BoldCommand,
    ItalicCommand,
    FcolorCommand,
    BGFColorCommand,
    VideoCommand

} from '../commands/commandEdit';
import insertVideo from "../modules/insertVideo";


class EditInterface {

    constructor( root ){
        this.edit = new CommandEdit();
        this.rootNode = root;
    }

    render(){

        let node = document.createElement('div');

        node.innerHTML = `
            <div  class="container">
                <div class="header">
                    <div class="nav">
                        <button title="Жирный" class="bold"><i class="fa fa-bold" aria-hidden="true"></i></button>
                        <button title="Курсив" class="italic"><i class="fa fa-italic" aria-hidden="true"></i></button>
                        <button title="Цвет шрифта" class="fcolor"><i class="fa fa-font" aria-hidden="true"></i></button>
                        <!--<button title="Цвет фона" class="bgfcolor"><i class="fa fa-tint" aria-hidden="true"></i></button>-->
                        <input type="color" id="color" style="width: 20px; height: 15px" title="Цвет фона"/>
                        <button title="Добавить Youtube видео" class="video"><i class="fa fa-youtube" aria-hidden="true"></i></button>
                        <button title="Копировать" class="copy"><i class="fa fa-copy" aria-hidden="true"></i></button>
                        
                     </div>
                     <div class="nav">
                        <button title="Отменить" class="undo" ><i class="fa fa-undo" aria-hidden="true"></i></button>
                        <!--<button title="Повторить" class="redo" ><i class="fa fa-repeat" aria-hidden="true"></i></button>-->
                        <button title="ellipsis" class="ellipsis"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></button>
                     </div>
                </div>
                <div id="edit" class="editable" contenteditable="true">
                  ${this.edit.getCurrentText()}
                </div>
                <div class="footer"><span class="characters"></span></div>
            </div>
        `;

        const editArea = node.querySelector('.editable');

        editArea.addEventListener("keypress", () => {
            let text = editArea.innerHTML;
            this.edit.execute( new TextCommand( text ) );
        });


        node.querySelector('.bold').addEventListener('click', () => {
            this.edit.execute( new BoldCommand( 'bold' ) );
        });

        node.querySelector('.italic').addEventListener('click', () => {
            this.edit.execute( new ItalicCommand( 'italic' ) );
        });

        node.querySelector('.fcolor').addEventListener('click', () => {
            document.execCommand('styleWithCSS', false, true);
            this.edit.execute( new FcolorCommand( "rgba(20,120,120,0.5)" ) );
        });

        node.querySelector('#color').addEventListener("change", event => {
            this.edit.execute( new BGFColorCommand(  event.target.value ) );
        }, false);


        node.querySelector('.video').addEventListener('click', () => {
            this.edit.execute( new VideoCommand( insertVideo ) );
        });


        node.querySelector('.copy').addEventListener('click', () => {

            const listener = event => {
                event.clipboardData.setData("text/html", editArea.innerHTML);
                event.clipboardData.setData("text/plain", editArea.innerHTML);
                event.preventDefault();
            };

            document.addEventListener("copy", listener);
            document.execCommand("copy");
            document.removeEventListener("copy", listener);
        });

        node.querySelector('.undo').addEventListener('click', () => {
             this.edit.undo();
             this.render();
        });

        this.rootNode.innerHTML = null;
        this.rootNode.appendChild( node );

    }
}

const application = () => {

    const root = document.querySelector('#root');
    let app = new EditInterface(root);

    app.render();

};


export default application;
