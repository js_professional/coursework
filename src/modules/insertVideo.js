const insertVideo = () => {
        let urlPrompt = prompt("Введите ссылку на Youtube видео :", "http://");
        let url = urlPrompt !== null ? urlPrompt.replace("watch?v=", "embed/") : "http://";
        let embed = `<iframe src='${url}' allowfullscreen="true" width="200" frameborder="0" height="200">`;

        if (embed != null) {
            document.execCommand("insertHTML", false, embed);
        }
};

export default insertVideo;
