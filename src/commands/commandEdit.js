export class CommandEdit{
    constructor(){
        this.currentValue = '';
        this.commandExecuted = [];
    }

    undo(){
        const command = this.commandExecuted.pop();

        if( command ) {
            this.currentValue = command.run( this.currentValue );
        }
    }

    execute( command ){
        if ( this.commandExecuted.length % 10 === 0 ) {
            this.currentValue = '';
            this.commandExecuted = [];
        }

        this.currentValue = command.run( this.currentValue );
        this.commandExecuted.push(command);
    }

    getCurrentText(){
        return this.currentValue;
    }
}

function Command(func, value){
    this.run = func;
    this.value = value;
}

function addText(){
    return this.value;
}
function bold(){
    return document.execCommand( this.value );
}

function italic(){
    return document.execCommand( this.value );
}

function fcolor(){
    return document.execCommand('foreColor', false, this.value)
}

function bgfcolor(){
   return document.execCommand('hiliteColor', false, this.value) ;
}

function video(){
    return this.value();
}

export const TextCommand = function( value ){
    Command.call( this, addText, value );
};

export const BoldCommand  = function( value ){
    Command.call( this, bold,  value );
};

export const VideoCommand  = function( value ){
    Command.call( this, video, value );
};

export const ItalicCommand  = function( value ){
    Command.call( this, italic, value );
};

export const FcolorCommand  = function( value ){
    Command.call( this, fcolor, value );
};

export const BGFColorCommand  = function( value ){
    Command.call( this, bgfcolor, value );
};


