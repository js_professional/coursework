const path = require('path');
const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');
const HTMLWebpackPlugin = require('html-webpack-plugin');



module.exports = merge(common, {
    output: {
        filename: 'bundle.[name].[hash].js',
        path: path.resolve(__dirname, 'public'),
    },
    mode: 'development',
    devtool: 'inline-source-map',

    devServer: {
        contentBase: path.join(__dirname, 'public'),
        compress: true,
        host: '127.0.0.1',
        port: 8080,
        hot: true,
        inline: true
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.html$/,
                use: [
                    'html-loader'
                ]
            }
        ]
    },
    plugins: [
        new HTMLWebpackPlugin({
            title: "Course",
            filename: 'index.html',
            template: './public/index.html',
            minify: {
                removeComments: true,
                collapseWhitespace: true
            },
            chunks: [
                'main'
            ]
        })
    ]
});

